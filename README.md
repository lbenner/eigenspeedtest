# EigenSpeedTest

This is simple benchmark for certain Eigen functionatliy. 

## Getting started

    git clone https://gitlab.com/lbenner/eigenspeedtest.git
    cd eigensspeedtest
    git submodule update --init --recursive
    mkdir build && cd build
    cmake -G Ninja -DCMAKE_BUILD_TYPE=Release ..
    cmake --build .
    ./EigenSpeed

## Getting another Eigen versin

  cd ../Eigen
  git checkout <eigen branch>
  
Used <eigen branch> values:

* 3.3.7
* 3.4.0
* master
