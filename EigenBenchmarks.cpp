﻿
#include <Eigen/Eigen>

#include <benchmark/benchmark.h>

#include <cstdlib>
#include <iostream>

double results[3] = {0, 0, 0};


static void BM_FillMatrix3d(benchmark::State& state)
{
  double res;

  Eigen::Matrix3d A;

  double var = (std::rand() % 1000) / 10.0;

  for (auto _ : state)
  {
    A << 1.0, 2.0, 3.0,
      4.0, var, 6.0,
      7.0, 8.0, 9.0;

    res = A(1,1);

    benchmark::DoNotOptimize(A);
  }

  results[0] = res;
}

BENCHMARK(BM_FillMatrix3d);


static void BM_EigenCheck(benchmark::State& state)
{
  double res = 0.0;
  
  Eigen::Matrix3d A;

  double var = (std::rand() % 1000) / 10.0;

  for (auto _ : state)
  {
    A << 1.0, 2.0, 3.0,
      4.0, var, 6.0,
      7.0, 8.0, 9.0;

    Eigen::Vector3d v(1.0, 2.0, 3.0);

    Eigen::Vector3d x = A * v;

    Eigen::Vector3d y = A.transpose() * v;

    Eigen::Vector3d d = x - y;

    var = A(1,1) + 0.00001;

    double r = d.norm();

    A.row(1) -= r * v;

    res = A(1, 1);

    benchmark::DoNotOptimize(A);
    
    benchmark::DoNotOptimize(res);
  }

  results[1] = res;
}

BENCHMARK(BM_EigenCheck);

static void BM_Out(benchmark::State& state)
{
  static int n = 0;

  ++n;

  for (auto _ : state)
  {
    results[2] = double(n);
  }

  std::cout << n << " [" << results[0] << "; "<< results[1] << "; "<< results[2] << "]" << std::endl;
}

BENCHMARK(BM_Out);

BENCHMARK_MAIN();
